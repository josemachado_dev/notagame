﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransformItems : MonoBehaviour
{

    int round = 1;
    bool isOnTrigger = false;
    bool hasUsedItemA, hasUsedItemB, hasUsedItemC, hasUsedItemD, hasUsedItemE, hasUsedItemF, hasUsedItemG = false;

    public Text text;
    public GameObject player;

	void Update(){
		if(hasUsedItemA && hasUsedItemB){
			round = 2;
			if(hasUsedItemD && hasUsedItemE){
				round = 3;
				if(hasUsedItemC && hasUsedItemF){
					round = 4;
				}
			}
		}	
	}

    void OnTriggerStay2D(Collider2D other)
    {
        isOnTrigger = true;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        isOnTrigger = false;
    }

    public void UseItem(string itemID)
    {
        if (isOnTrigger)
        {
            switch (itemID)
            {
                case "A":
                    player.GetComponent<PlayerManager>().hasItemA = false;
                    hasUsedItemA = true;
                    break;
                case "B":
                    player.GetComponent<PlayerManager>().hasItemB = false;
                    hasUsedItemB = true;
                    break;
                case "C":
                    player.GetComponent<PlayerManager>().hasItemC = false;
                    hasUsedItemC = true;
                    break;
                case "D":
                    player.GetComponent<PlayerManager>().hasItemD = false;
                    hasUsedItemD = true;
                    break;
                case "E":
                    player.GetComponent<PlayerManager>().hasItemE = false;
                    hasUsedItemE = true;
                    break;
                case "F":
                    player.GetComponent<PlayerManager>().hasItemF = false;
                    hasUsedItemF = true;
                    break;
            }
        }
    }
}
