﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{

    [HideInInspector]
    public bool hasItemA, hasItemB, hasItemC, hasItemD, hasItemE, hasItemF, hasItemG;

    public GameObject ItemA, ItemB, ItemC, ItemD, ItemE, ItemF, ItemG;

    void Start()
    {

    }

    void Update()
    {
        #region Inventory Show items in inventory
        if (hasItemA)
            ItemA.SetActive(true);
        else
            ItemA.SetActive(false);

        if (hasItemB)
            ItemB.SetActive(true);
        else
            ItemB.SetActive(false);

        if (hasItemC)
            ItemC.SetActive(true);
        else
            ItemC.SetActive(false);

        if (hasItemD)
            ItemD.SetActive(true);
        else
            ItemD.SetActive(false);

        if (hasItemE)
            ItemE.SetActive(true);
        else
            ItemE.SetActive(false);

        if (hasItemF)
            ItemF.SetActive(true);
        else
            ItemF.SetActive(false);

        if (hasItemG)
            ItemG.SetActive(true);
        else
            ItemG.SetActive(false);
        #endregion

    }
}
