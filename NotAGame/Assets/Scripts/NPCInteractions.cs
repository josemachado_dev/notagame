﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCInteractions : MonoBehaviour
{

    //What NPC is this?
    public enum NPC_ID
    {
        Male_Ghost,
        Female_Ghost,
    }
    public NPC_ID NPC_is;

    public GameObject player;

    public GameObject ItemA_Menu, ItemB_Menu, ItemG_Menu, ItemF_Menu;

    Animator animator;

    bool hasGivenItemA = false;
    bool hasGivenItemB = false;

    bool hasReceivedItemG = false;
    bool hasReceivedItemF = false;

    public void TalkToPC(GameObject NPC)
    {
        NPC.gameObject.GetComponent<Animator>().SetBool("isTalkingToPC", true);

        //Has not given Item A (Male Ghost)
        if (!NPC.GetComponent<NPCInteractions>().hasGivenItemA && NPC.GetComponent<NPCInteractions>().NPC_is == NPC_ID.Male_Ghost)
        {
            NPC.GetComponent<NPCInteractions>().ItemA_Menu.SetActive(true);
            player.GetComponent<PlayerManager>().hasItemA = true;
        }

        //Has not given Item B (Female Ghost)
        if (!NPC.GetComponent<NPCInteractions>().hasGivenItemB && NPC.GetComponent<NPCInteractions>().NPC_is == NPC_ID.Female_Ghost)
        {
            NPC.GetComponent<NPCInteractions>().ItemB_Menu.SetActive(true);
            player.GetComponent<PlayerManager>().hasItemB = true;
        }

        //Has not recieved Item G (Male Ghost)
        if (NPC.GetComponent<NPCInteractions>().hasGivenItemA && !NPC.GetComponent<NPCInteractions>().hasReceivedItemG && NPC.GetComponent<NPCInteractions>().NPC_is == NPC_ID.Male_Ghost)
        {
            NPC.GetComponent<NPCInteractions>().ItemG_Menu.SetActive(true);
            player.GetComponent<PlayerManager>().hasItemG = true;
        }

        //Has not given Item F (Female Ghost)
        if (NPC.GetComponent<NPCInteractions>().hasGivenItemB && !NPC.GetComponent<NPCInteractions>().hasReceivedItemF && NPC.GetComponent<NPCInteractions>().NPC_is == NPC_ID.Female_Ghost)
        {
            NPC.GetComponent<NPCInteractions>().ItemF_Menu.SetActive(true);
            player.GetComponent<PlayerManager>().hasItemF = true;
        }

    }

    public void GiveItem(string item)
    {
        switch (item)
        {
            case "A":
                hasGivenItemA = true;
                ItemA_Menu.SetActive(true);
                gameObject.GetComponent<Animator>().SetBool("isTalkingToPC", false);
                break;
            case "B":
                hasGivenItemB = true;
                ItemB_Menu.SetActive(true);
                gameObject.GetComponent<Animator>().SetBool("isTalkingToPC", false);
                break;
        }
    }

    public void ReceiveItem(string item)
    {
        switch (item)
        {
            case "F":
                hasReceivedItemF = true;
                ItemF_Menu.SetActive(true);
                gameObject.GetComponent<Animator>().SetBool("isTalkingToPC", false);
                break;
            case "G":
                hasReceivedItemG = true;
                ItemG_Menu.SetActive(true);
                gameObject.GetComponent<Animator>().SetBool("isTalkingToPC", false);
                break;
        }
    }
}
