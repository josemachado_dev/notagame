﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

	public void InventoryButton(GameObject inventory){
		inventory.SetActive(!inventory.activeInHierarchy);
	}
}
